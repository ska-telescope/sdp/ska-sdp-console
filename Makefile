include .make/base.mk
include .make/oci.mk
include .make/dependencies.mk

PROJECT_NAME = ska-sdp-console
PROJECT_PATH = ska-telescope/sdp/ska-sdp-console
ARTEFACT_TYPE = oci

CHANGELOG_FILE = CHANGELOG.rst