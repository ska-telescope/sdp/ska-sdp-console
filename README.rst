SDP Console
===========

The Dockerfile in this repository builds an image that can be used to
launch a bash or iPython shell to connect to the SDP configuration
database.

Standard CI machinery
---------------------

This repository is set up to use the
`Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`__
and `CI jobs <https://gitlab.com/ska-telescope/templates-repository>`__
maintained by the System Team. For any questions, please look at the
documentation in those repositories or ask for support on Slack in the
#team-system-support channel.

To keep the Makefiles up to date in this repository, follow the
instructions at:
https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile#keeping-up-to-date

Creating a new release
----------------------

When you are ready to make a new release:

-  Check out the master branch
-  Update the version number in ``.release`` with

   -  ``make bump-patch-release``,
   -  ``make bump-minor-release``, or
   -  ``make bump-major-release``

-  Update the ``CHANGELOG.md`` and add the right version number
-  Create the git tag with ``make git-create-tag``
-  Push the changes with ``make git-push-tag``
