.. Hidden toctree to manage the sidebar navigation.
.. toctree::
  :maxdepth: 2
  :caption: Home
  :hidden:


SDP Console
===========

The SDP Console is one of the :doc:`components <ska-sdp-integration:design/components>`
that make up the SDP system.

The Dockerfile in the Console repository builds an image that can be used to launch a command-line environment
(i.e. a bash or iPython shell) to connect to, and interact with, the SDP configuration database.
This enables the use of the :doc:`configuration library CLI <ska-sdp-config:cli>`
via its ``ska-sdp`` command.

.. warning::

    The configuration library CLI is not recommended for non-developers -
    the :doc:`iTango interface <ska-sdp-integration:operation/itango>`
    is an easier way to work with SDP for general users.

A quick recipe for working with the Console and the SDP CLI can be found
:doc:`here <ska-sdp-integration:operation/console>`.
