Changelog
=========

1.0.0
-----

-  Updated dependencies for list, see pyproject.toml on the MR
   (`MR42 <https://gitlab.com/ska-telescope/sdp/ska-sdp-console/-/merge_requests/42>`__)
-  Update Dockerfile to use SKA Python base image
   (`MR35 <https://gitlab.com/ska-telescope/sdp/ska-sdp-console/-/merge_requests/35>`__)
-  Removed RTD html_static_path
   (`MR37 <https://gitlab.com/ska-telescope/sdp/ska-sdp-console/-/merge_requests/37>`__)
-  Add RTD documentation
   (`MR36 <https://gitlab.com/ska-telescope/sdp/ska-sdp-console/-/merge_requests/36>`__)

0.8.0
-----

-  Use ska-sdp-config 0.10.1
   (`MR34 <https://gitlab.com/ska-telescope/sdp/ska-sdp-console/-/merge_requests/34>`__)

.. _section-1:

0.7.0
-----

-  Use ska-sdp-config 0.9.0 and switched to use ska-sdp-python base
   image
   (`MR31 <https://gitlab.com/ska-telescope/sdp/ska-sdp-console/-/merge_requests/31>`__)

.. _section-2:

0.6.0
-----

-  Use ska-sdp-config 0.6.0

.. _section-3:

0.5.2
-----

-  Use ska-sdp-config 0.5.6

.. _section-4:

0.5.1
-----

-  Use ska-sdp-config 0.5.3

.. _section-5:

0.5.0
-----

-  Use ska-sdp-config 0.5.2

.. _section-6:

0.4.5
-----

-  Use ska-sdp-config 0.4.5

.. _section-7:

0.4.4
-----

-  Use ska-sdp-config 0.4.4

.. _section-8:

0.4.3
-----

-  Use ska-sdp-config 0.4.3

.. _section-9:

0.3.2
-----

-  Ported to use the latest version ska-sdp-config package (0.3.3)
