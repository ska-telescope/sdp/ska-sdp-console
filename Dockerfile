FROM artefact.skao.int/ska-build-python:0.1.1 as build

WORKDIR /build

COPY . ./

ENV POETRY_VIRTUALENVS_CREATE=false

RUN poetry install --no-root --only main

FROM artefact.skao.int/ska-python:0.1.2

COPY --from=build /usr/local/ /usr/local/

WORKDIR /app

CMD ["/bin/bash", "-c", "trap : INT TERM; sleep infinity & wait"]
